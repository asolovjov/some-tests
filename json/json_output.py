import argparse
import sys
import json

def print_object(indent, json_obj, is_list_item = False, list_prefix = "      - "):
    if isinstance(json_obj, dict):
        if is_list_item:
            print()
            print("\t" * (indent - 2) + list_prefix, end="")
            prefix = "\t" * (indent - 1)
        else:
            prefix = "\t" * indent
            indent = indent + 1
        for key, value in json_obj.items():
            print(f"\n{prefix}{key} : ", end = "")
            print_object(indent, value, False, list_prefix)
    elif isinstance(json_obj, list):
        if is_list_item:
            print("\n" + "\t" * (indent - 2) + list_prefix, end="")
        prefix = "\t" * indent
        indent = indent + 1
        for list_item in json_obj:
            print_object(indent, list_item, True, list_prefix)
    else:
        if is_list_item:
            prefix = "\t" * (indent - 2) + list_prefix
            print(f"\n{prefix}{json_obj}", end="")
        else:
            print(f"{json_obj}", end="")

parser = argparse.ArgumentParser()
parser.add_argument("-j", "--json", required=True, help="JSON File")

args = parser.parse_args()
json_file_path = args.json
print(f"Using {json_file_path}")
try:
    with open(json_file_path, 'r') as file:
        file_contents = file.read()
        file.close
except FileNotFoundError:
    print(f"File {json_file_path} doesn't exist")
    sys.exit(1)
except Exception as e:
    print(f"Error opening file {json_file_path}")

json_dict = json.loads(file_contents)
print_object(0, json_dict, False, "      - ")
print()
