import requests
import json
import os

api_key = os.environ.get('API_KEY')
country="RU"
city="Moscow"
base_url="https://api.weatherbit.io/v2.0/current?"
url=f'{base_url}city={city}&country={country}&key={api_key}'
response = requests.get(url)
if response.status_code == 200:
    json_response = response.json()
else:
    print("Error: ", response.status_code)

print("Температура в Москве:", json_response['data'][0]['temp'])
